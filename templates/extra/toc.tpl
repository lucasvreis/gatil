<div id="table-of-contents">
  <details open>
    <summary><b><toc-name /></b></summary>
    <o:scope>
      <o:bind org:sections>
        <ul style="list-style-type:none">
          <this.list as=section>
            <li>
              <details>
                <summary><a href="!(page:route)#!(section:anchor)"><section:title /></a></summary>
                <section:subsections/>
              </details>
            </li>
          </this.list>
        </ul>
      </o:bind>
      <doc:sections/>
    </o:scope>
  </details>
</div>
