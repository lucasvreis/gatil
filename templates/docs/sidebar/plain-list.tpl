<o:bind snd-order-list>
  <ol>
    <this.items:list>
      <li class="section">
        <item.content/>
      </li>
    </this.items:list>
  </ol>
</o:bind>

<this.items:list>
  <li class="section">
    <o:scope>
      <o:bind org:element:plain-list><snd-order-list/></o:bind>
      <item.content/>
    </o:scope>
  </li>
</this.items:list>
