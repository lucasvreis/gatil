<o:match parent.route>
  <case !(this.target)>
    <a style="font-weight: 500;" href="!(this.target)"><this.content/></a>
    <query route="!(this.target)">
      <q:result>
        <o:scope>
          <o:bind org:sections>
            <ol>
              <this.list as="section">
                <li class="section">
                  <a href="!(link.target)#!(section.anchor)">
                    <section:title/>
                  </a>
                  <section:subsections/>
                </li>
              </this.list>
            </ol>
          </o:bind>
          <o:with link=this>
            <doc:sections/>
          </o:with>
        </o:scope>
      </q:result>
    </query>
  </case>
  <default>
    <a href="!(this.target)"><this.content/></a>
  </default>
</o:match>
