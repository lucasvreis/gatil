<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/katex@0.16.2/dist/katex.min.css" integrity="sha384-bYdxxUwYipFNohQlHt0bjN/LCpueqWz13HufFEV1SUatKs1cm4L6fFgCi1jT643X" crossorigin="anonymous">
<script defer src="https://cdn.jsdelivr.net/npm/katex@0.16.2/dist/katex.min.js" integrity="sha384-Qsn9KnoKISj6dI8g7p1HBlNpVx0I8p1SvlwOldgi3IorMle61nQy4zEahWYtljaz" crossorigin="anonymous"></script>
<script defer src="https://cdn.jsdelivr.net/npm/katex@0.16.2/dist/contrib/auto-render.min.js" integrity="sha384-+VBxd3r6XgURycqtZ117nYw44OOcIax56Z4dCRWbxyPt0Koah1uHoK0o4+/RRE05" crossorigin="anonymous"></script>
<script defer src="https://cdn.jsdelivr.net/npm/katex@0.16.2/dist/contrib/copy-tex.min.js" integrity="sha384-ww/583aHhxWkz5DEVn6OKtNiIaLi2iBRNZXfJRiY1Ai7tnJ9UXpEsyvOITVpTl4A" crossorigin="anonymous"></script>
<script>
 var katexOptions = () => ({
   delimiters: [
     {left: '$$', right: '$$', display: true},
     {left: '$', right: '$', display: false},
     {left: '\\(', right: '\\)', display: false},
     {left: '\\[', right: '\\]', display: true}
   ],
   macros: {
     "\\NN": "\\mathbb{N}",
     "\\ZZ": "\\mathbb{Z}",
     "\\QQ": "\\mathbb{Q}",
     "\\RR": "\\mathbb{R}",
     "\\DeclarePairedDelimiter": "\\providecommand{#1}[1]{#2 ##1 #3}",
     "\\abs": "\\lvert #1 \\rvert",
     "\\CC": "\\mathbb{C}",
     "\\interval": context => {
       let arg = context.consumeArg(["]"]);
       let argtext = arg.tokens
                        .reduce((p, c) => c.text + p, "")
                        .slice(1);
       let a = "["
       let b = "]"
       switch (argtext) {
         case 'open':
           a = "("
           b = ")"
           break;
         case 'open left':
           a = "("
           break;
         case 'open right':
           b = ")"
           break;
       }
       return a + "#1,#2" + b;
     }
   },
   trust: true,
   globalGroup: true,
   throwOnError: false
 });

 function renderMathInRoot(root) {
   let opts = katexOptions();
   let walls = Array.from(root.querySelectorAll('.macrowall'));
   let mathElements =
     Array.from(root.querySelectorAll('span.math, div.math, script.math'))
          .filter(e =>
            !walls.some(i => i.contains(e))
          );
   for (let element of mathElements) {
     if (element.tagName == 'SCRIPT') {
       katex.renderToString(element.textContent, opts);
     } else {
       let display = element.classList.contains('display');
       katex.render(element.textContent, element, {
         ...opts,
         displayMode: display,
       });
     }
   }
   document.dispatchEvent(new CustomEvent('MathRendered'));
 }

 function renderMath() {
   let walls = document.querySelectorAll('.macrowall');
   renderMathInRoot(document);
   for (let root of walls) {
     renderMathInRoot(root);
   }
 }

 document.addEventListener("DOMContentLoaded", () => {
   renderMath();

   window.addEventListener("EMAHotReload", () => {
     renderMath();
   })
 })
</script>

<o:bind-text katex-preamble>
  <math:getmacros @try>
    \providecommand{\<macro:key/>}[<macro:value.args/>]{\htmlClass{macro-scope}{\htmlData{name=<macro:key/>,origin=<page:route/>}{<macro:value.body/>}}}
  </math:getmacros>
</o:bind-text>
<script class="math preamble" type="tex">!(katex-preamble)</script>

<style type="text/css" media="screen">
 .katex { font-size: 0.93em; }
 .katex .macro-scope:hover {
     background: #0dacbe33;
     background-size: 50%;
     border-radius: 2px;
 }
</style>
