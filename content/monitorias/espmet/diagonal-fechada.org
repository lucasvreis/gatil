#+title: A diagonal é fechada
#+latex_compiler: lualatex
#+options: toc:nil num:nil
#+beamer_color_theme: dove
#+latex_header: \usepackage{mathtools}
#+latex_header: \usepackage{tikz}
#+latex_header: \usetikzlibrary{arrows.meta}

* Motivação
Lendo os exercícios de continuidade para a monitoria, notei que alguns dos problemas são consequências relativamente simples de um mesmo fato, que foi provado como exercício lá no capítulo 2, mas que é bastante importante do ponto de vista topológico. Achei que seria interessante explicar melhor o que esse fato significa, e a relação entre todos eles.

* O exercício
O exercício em questão é o 18 do capítulo 2. Nele, mostramos que em todo espaço métrico \(M\), todo ponto \(p \notin \Delta\) fora da "diagonal" \(\Delta = \{ (x,x) : x \in M \} \subseteq M \times M\) tem distância \(d(p,\Delta) > 0\). Em outras palavras, para todo \(p\) dessa forma, existe uma bola aberta centrada em \(p\) e contida em \(\Delta^{\complement}\). Em menos palavras, \(\Delta\) é um subconjunto fechado.

#+begin_center
\begin{tikzpicture}[thick]
  \draw
  (0,0) --node[below] {$M$} (2,0)
  (0,0) --node[left] {$M$} (0,2);
  \draw[scale=0.9,thin] (0,0) -- (2,2) node[above right] {$\Delta$};
  \draw[dashed, gray!50!black, fill opacity=0.2, fill=gray]
  (0.8, 1.6) circle[radius=0.38*sqrt(2)];
  \fill (0.8, 1.6)
  circle[radius=1pt]
  node[above, inner sep=0.5mm] {$p$};
\end{tikzpicture}
#+end_center

* Geometricamente...
O interessante dessa propriedade é que ela é equivalente a existirem, para cada par \(x,y \in M\) com \(x \neq y\), duas vizinhanças \(B(x,\varepsilon_1)\) e \(B(y,\varepsilon_2)\) que são disjuntas entre si[fn::não vamos usar esse fato nos exercícios, mas é interessante tentar mostrar.]. Ou seja, a propriedade nos diz que pontos distintos de um espaço métrico estão "separados" em um certo sentido. Em um curso de topologia geral, vocês verão que isso nem sempre é verdade em um espaço topológico (que não é métrico). Os espaços que têm essa propriedade têm até um nome, são chamados de Hausdorff.

#+begin_center
\begin{tikzpicture}[thick]
  \draw
  (0,0) -- (2,0)
  (0,0) -- (0,2);
  \draw[scale=0.9,thin] (0,0) -- (2,2) node[above right] {$\Delta$};
  \draw[dashed, gray!50!black, fill opacity=0.2, fill=gray]
  (0.8 + 0.4, 1.6 + 0.4)
  -- (0.8 - 0.4, 1.6 + 0.4)
  -- (0.8 - 0.4, 1.6 - 0.4)
  -- (0.8 + 0.4, 1.6 - 0.4)
  -- cycle;
  \draw[thin, dotted] (0.8,0) -- (0.8,1.6);
  \draw[Parenthesis-Parenthesis] (0.4,0) -- (1.2,0);
  \fill (0.8,0) circle[radius=1pt] node[below] {$x$};
  \draw[thin, dotted] (0,1.6) -- (0.8,1.6);
  \draw[Parenthesis-Parenthesis] (0,1.2) -- (0,2.0);
  \fill (0,1.6) circle[radius=1pt] node[left] {$y$};
  \fill (0.8, 1.6) circle[radius=1pt] node[above, scale=0.6] {$(x,y)$};
\end{tikzpicture}
#+end_center

* Os exercícios de continuidade
Vamos ver como esse fato dá demonstrações relativamente simples de alguns exercícios do capítulo de continuidade. 

** Hygino, cap. 4
10. [@10] Sejam \(M\) e \(N\) espaços métricos e \(f,g \colon M \to N\) funções contínuas. Se \(f(a) \neq g(a)\), para algum \(a \in M\), mostre que existe uma bola \(B = B(a, \varepsilon)\) tal que \(f(x) \neq g(y)\) para quaisquer \(x,y \in B\).
    
#+beamer: \vspace{5pt}
Como de costume, existem várias formas de fazer o exercício. Apresentamos aqui um "roteiro" de uma solução relativamente curta, usando a propriedade da diagonal \(\Delta\) ser fechada.
    
* Roteiro

#+attr_beamer: :overlay <+->
1. Defina \(h : M \times M \to N \times N\) por \(h(x,y) = (f(x),g(y))\). A função \(h\) é contínua? (dica: use a proposição 7 da seção 2).
2. Seja \(\Delta_N = \{ (z,z) : z \in N \}\). Explique por que \(H \coloneqq h^{-1}(\Delta_N)\) é exatamente o conjunto dos pares \((x,y) \in M \times M\) tais que \(f(x) = f(y)\), e com base no que foi discutido acima, conclua que \(H\) é fechado.
3. Assim, se \(a \in M\) é tal que \(f(a) \neq g(a)\), então \((a,a) \notin H\).
   Use o fato do complementar de \(H\) ser aberto para mostrar que existe \(\varepsilon\) tal que \(B(a,\varepsilon) \times B(a,\varepsilon) \subseteq H^{\complement}\).
4. Verifique que \(B = B(a,\varepsilon)\) satisfaz o enunciado.
    
* Nota :noexport:
Essa solução é até um pouco mais geral que o exercício: se \(x,y \in M\) forem tais que \(f(x) \ne g(y)\), pelo mesmo argumento temos \((x,y) \notin H\), da onde segue que existe um par de vizinhanças \(B_1 = B(x,\varepsilon), B_2 = B(y,\varepsilon)\) tais que \(f(z) \neq g(w)\) para todos \(z \in B_1\), \(w \in B_2\).

* Mais exercícios
:PROPERTIES:
:BEAMER_opt: t
:END:
** Lista 2 do Tozoni
2. [@2] Seja \(f : M \to M'\) uma função contínua. Mostre que o gráfico \(G(f) = \{ (x,f(x)) : x \in M \}\) de \(f\) é fechado.

#+beamer: \pause\vspace{30pt}

Roteiro:
#+attr_beamer: :overlay <+->
1. Seja \(\Delta_{M'} = \{ (x,x) : x \in M' \}\). Novamente, ele é um subconjunto fechado. Defina \(h : M \times M' \to M' \times M'\) por \(h(x,y) = (f(x),y)\). \(h\) é uma função contínua?
2. Mostre que \(h^{-1}(\Delta_{M'}) = G(f)\). Conclua que \(G(f)\) é fechado.

* Mais exercícios
:PROPERTIES:
:BEAMER_opt: t
:END:
** Lista 2 do Tozoni
3. [@3] Sejam \(f,g : M \to M'\) funções contínuas. Mostre que o conjunto \(A = \{ x \in M : f(x) = g(x) \}\) é fechado.

#+beamer: \pause\vspace{30pt}

Roteiro:
#+attr_beamer: :overlay <+->
1. Novamente, seja \(\Delta_{M'} = \{ (x,x) : x \in M' \}\). Defina \(h : M \to M' \times M'\) por \(h(x) = (f(x),g(x))\).
2. Mostre que \(h^{-1}(\Delta_{M'}) = A\). Conclua que \(A\) é fechado.

* Mais exercícios
:PROPERTIES:
:BEAMER_opt: t
:END:
** Lista 2 do Tozoni
4. [@4] Sejam \(f,g : M \to M'\) funções contínuas e \(Q \subset M\) um conjunto denso. Mostre que se para todo \(x \in Q\), \(f(x) = g(x)\), então \(f = g\).

#+beamer: \pause\vspace{30pt}

Dica:
#+attr_beamer: :overlay <+->
1. Chame de \(A\) o conjunto \(\{ x \in M : f(x) = g(x) \}\) (como no exercício anterior). Queremos mostrar que \(A = M\) (por que?).
2. Já mostramos que \(A\) é fechado, e sabemos que \(Q \subset A\). Assim, use o fato que \(\overline{Q} = M\) e \(\overline{A} = A\) para concluir.
