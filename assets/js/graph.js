// Based on: https://observablehq.com/@d3/force-directed-graph
// Copyright 2021 Observable, Inc.
// Released under the ISC license.
// This version was modified by lucasvreis

function changeTooltip (obj, html) {
  let el = obj.node();
  MathJax.typesetClear([el]);
  obj.html(html);
  MathJax.startup.promise =
    MathJax.startup.promise
           .then(() => MathJax.typesetPromise([el]));
}

function ForceGraph(
  {
    nodes, // an iterable of node objects (typically [{id}, …])
    links, // an iterable of link objects (typically [{source, target}, …])
  },
  {
    nodeId = (d) => d.id, // given d in nodes, returns a unique identifier (string)
    nodeGroup = (d) => d.id, // given d in nodes, returns an (ordinal) value for color
    nodeGroups, // an array of ordinal values representing the node groups
    nodeTitle, // given d in nodes, a title string
    nodeFill = "currentColor", // node stroke fill (if not using a group color encoding)
    nodeStroke = "#fffdf8", // node stroke color
    nodeStrokeWidth = 1.5, // node stroke width, in pixels
    nodeStrokeOpacity = 1, // node stroke opacity
    nodeRadius = 5, // node radius, in pixels
    nodeStrength,
    linkSource = ({ source }) => source, // given d in links, returns a node identifier string
    linkTarget = ({ target }) => target, // given d in links, returns a node identifier string
    linkStroke = "#999", // link stroke color
    linkStrokeOpacity = 0.6, // link stroke opacity
    linkStrokeWidth = 0.8, // given d in links, returns a stroke width in pixels
    linkStrokeLinecap = "round", // link stroke linecap
    linkStrength,
    colors = d3.schemeTableau10, // an array of color strings, for the node groups
    invalidation, // when this promise resolves, stop the simulation
  } = {}
) {
  // Compute values.
  const N = d3.map(nodes, nodeId).map(intern);
  const LS = d3.map(links, linkSource).map(intern);
  const LT = d3.map(links, linkTarget).map(intern);
  if (nodeTitle === undefined) nodeTitle = (_, i) => N[i];
  const T = nodeTitle == null ? null : d3.map(nodes, nodeTitle);
  const G = nodeGroup == null ? null : d3.map(nodes, nodeGroup).map(intern);
  const W =
    typeof linkStrokeWidth !== "function"
      ? null
      : d3.map(links, linkStrokeWidth);

  // Replace the input nodes and links with mutable objects for the simulation.
  nodes = d3.map(nodes, (_, i) => ({ id: N[i] }));
  links = d3.map(links, (_, i) => ({ source: LS[i], target: LT[i] }));

  // Compute default domains.
  if (G && nodeGroups === undefined) nodeGroups = d3.sort(G);

  // Construct the scales.
  const color = nodeGroup == null ? null : d3.scaleOrdinal(nodeGroups, colors);

  // Construct the forces.
  const forceNode = d3.forceManyBody();
  const forceLink = d3.forceLink(links).id(({ index: i }) => N[i]);
  if (nodeStrength !== undefined) forceNode.strength(nodeStrength);
  if (linkStrength !== undefined) forceLink.strength(linkStrength);

  const simulation = d3
    .forceSimulation(nodes)
    .force("link", forceLink)
    .force("charge", forceNode)
    .force("center", d3.forceCenter())
    .force("x", d3.forceX())
    .force("y", d3.forceY())
    .alphaTarget(0.3)
    .on("tick", ticked);

  const gotoLink = (d) => {location.href = '/' + d.id};

  const tooltip = d3
    .select("body")
    .append("div")
    .style("display", "none")
    .attr("class", "tooltip")
    .style("background-color", "rgba(4, 4, 4, 0.6)")
    .style("font-family", "sans")
    .style("color", "white")
    .style("border-radius", "5px")
    .style("padding", "5px")
    .style("position", "absolute")
    .style("z-index", "1000");

  var dragging = false;

  // Three function that change the tooltip when user hover / move / leave a cell
  const mouseover = function (e, obj) {
    if (!dragging) {
      changeTooltip(tooltip, T[obj.index]);
      mousemove(e, obj);
      tooltip.style("display", "block");
      d3.select(this).attr("stroke", "black");
    }
  };
  const mousemove = function (e, { index: i }) {
    tooltip.style("display", "none");
    tooltip
      .style("left", e.pageX + 10 + "px")
      .style("top", e.pageY + "px");
    tooltip.style("display", "block");
  };

  const mouseleave = function () {
    tooltip.style("display", "none");
    d3.select(this).attr("stroke", "none");
  };

  const svg = d3
    .create("svg")
    .attr("preserveAspectRatio", "xMinYMin meet")
    .attr("viewBox", "-150 -150 300 300")
    .attr("style", "max-width: 100%; height: auto; height: intrinsic;");

  const trans = svg.append("g");

  const zoom = d3.zoom().on("zoom", handleZoom);

  function handleZoom(e) {
    trans.attr("transform", e.transform);
    if (e.sourceEvent != "whell") {
      tooltip.style("display", "none");
      node.attr("stroke", "none");
    }
  }

  svg.call(zoom);

  const link = trans
    .append("g")
    .attr("stroke", linkStroke)
    .attr("stroke-opacity", linkStrokeOpacity)
    .attr(
      "stroke-width",
      typeof linkStrokeWidth !== "function" ? linkStrokeWidth : null
    )
    .attr("stroke-linecap", linkStrokeLinecap)
    .selectAll("line")
    .data(links)
    .join("line");

  const node = trans
    .append("g")
    .attr("fill", nodeFill)
    .attr("cursor", "pointer")
    .selectAll("circle")
    .data(nodes)
    .join("circle")
    .attr("r", nodeRadius)
    .on("pointerup", function (e, d) {
      if (!dragging) {
        if (
          e.pointerType == "mouse" ||
          d3.select(this).style("stroke") != "none"
        )
          gotoLink(d);
        else mouseover.call(this, e, d);
      }
    })
    .on("mousemove", mousemove)
    .on("mouseleave", mouseleave);

  node.call(drag(simulation)).on("pointerover", function (e, obj) {
    if (e.pointerType == "mouse") {
      mouseover.call(this, e, obj);
    }
  });

  if (W) link.attr("stroke-width", ({ index: i }) => W[i]);
  if (G) node.attr("fill", ({ index: i }) => color(G[i]));
  // if (T) node.append("title").text(({index: i}) => T[i]);
  if (invalidation != null) invalidation.then(() => simulation.stop());

  function intern(value) {
    return value !== null && typeof value === "object"
      ? value.valueOf()
      : value;
  }

  function ticked() {
    link
      .attr("x1", (d) => d.source.x)
      .attr("y1", (d) => d.source.y)
      .attr("x2", (d) => d.target.x)
      .attr("y2", (d) => d.target.y);

    node.attr("cx", (d) => d.x).attr("cy", (d) => d.y);
  }

  function drag(simulation) {
    function dragstarted(event) {
      if (event.identifier == "mouse") {
        if (!event.active) simulation.alphaTarget(0.3).restart();
        event.subject.fx = event.subject.x;
        event.subject.fy = event.subject.y;
      }
      tooltip.style("display", "none");
    }

    function dragged(event) {
      if (event.identifier == "mouse") {
        event.subject.fx = event.x;
        event.subject.fy = event.y;
      }
      dragging = true;
    }

    function dragended(event) {
      if (event.identifier == "mouse") {
        if (!event.active) simulation.alphaTarget(0);
        event.subject.fx = null;
        event.subject.fy = null;
      }
      dragging = false;
    }

    return d3
      .drag()
      .on("start", dragstarted)
      .on("drag", dragged)
      .on("end", dragended);
  }
  for (i = 0; i < 300; i++) {
    simulation.tick();
  }
  simulation.alphaDecay(0.1).alpha(0).alphaTarget(0);
  return Object.assign(svg.node(), { scales: { color } });
}
