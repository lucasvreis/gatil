function setHtml(elm, html) {
    window.dispatchEvent(new Event('EMABeforeMorphDOM'));
    elm.innerHTML = html;
    window.dispatchEvent(new Event('EMAHotReload'));
}

// Ema Status indicator
const messages = {
    connected: "Connected",
    reloading: "Reloading",
    connecting: "Connecting to the server",
    disconnected: "Disconnected - try reloading the window"
};
function setIndicators(connected, reloading, connecting, disconnected) {
    const is = { connected, reloading, connecting, disconnected }

    for (const i in is) {
        document.getElementById(`ema-${i}`).style.display =
            is[i] ? "block" : "none"
        if (is[i])
            document.getElementById('ema-message').innerText = messages[i]
    }
    document.getElementById("ema-indicator").style.display = "block";
}
window.connected = () => setIndicators(true, false, false, false)
window.reloading = () => setIndicators(false, true, false, false)
window.connecting = () => setIndicators(false, false, true, false)
window.disconnected = () => setIndicators(false, false, false, true)
window.hideIndicator = () => {
    document.getElementById("ema-indicator").style.display = "none";
};

// Base URL path - for when the ema site isn't served at "/"
const baseHref = document.getElementsByTagName("base")[0]?.href;
const basePath = baseHref ? new URL(baseHref).pathname : "/";

// Use TLS for websocket iff the current page is also served with TLS
const wsProto = window.location.protocol === "https:" ? "wss://" : "ws://";
const wsUrl = wsProto + window.location.host + basePath;

// WebSocket logic: watching for server changes & route switching
function initShim(reconnecting) {
    // The route current DOM is displaying
    let routeVisible = document.location.pathname;

    const verb = reconnecting ? "Reopening" : "Opening";
    console.log(`ema: ${verb} conn ${wsUrl} ...`);
    window.connecting();
    const ws = new WebSocket(wsUrl);

    function switchRoute(path) {
        console.log(`ema: → Switching to ${path}`);
        window.history.pushState({}, "", path);

        let loc = window.location;

        // Don't waste resources switching to the same page
        if (routeVisible != loc.pathname) {
            sendObservePath(loc.pathname);
            routeVisible = loc.pathname;
        } else if (loc.href.indexOf('#')) {
            scrollToAnchor(loc.hash);
        }
    }

    function scrollToAnchor(hash) {
        console.log(`ema: Scroll to ${hash}`)
        if (hash == "") {
            window.scrollTo({ top: 0 });
        } else {
            var el = document.querySelector(hash);
            if (el !== null) {
                el.scrollIntoView();
            }
        }
    };

    function sendObservePath(path) {
        const relPath = path.startsWith(basePath) ? path.slice(basePath.length) : path;
        console.debug(`ema: requesting ${relPath}`);
        ws.send(relPath);
    }

    function watchCurrentRoute() {
        console.log(`ema: ⏿ Observing changes to ${document.location.pathname}`);
        sendObservePath(document.location.pathname);
    }

    ws.onopen = () => {
        console.log(`ema: ... connected!`);
        // window.connected();
        window.hideIndicator();
        watchCurrentRoute();
    };

    ws.onclose = () => {
        console.log("ema: reconnecting ..");
        window.reloading();
        // Reconnect after as small a time is possible, then retry again.
        // ghcid can take 1s or more to reboot. So ideally we need an
        // exponential retry logic.
        //
        // Note that a slow delay (200ms) may often cause websocket
        // connection error (ghcid hasn't rebooted yet), which cannot be
        // avoided as it is impossible to trap this error and handle it.
        // You'll see a big ugly error in the console.
        setTimeout(function () { initShim(true); }, 1);
    };

    ws.onmessage = evt => {
        if (evt.data.startsWith("REDIRECT ")) {
            console.log("ema: redirect");
            document.location.href = evt.data.slice("REDIRECT ".length);
        } else if (evt.data.startsWith("SWITCH ")) {
            console.log("ema: switch");
            switchRoute(evt.data.slice("SWITCH ".length));
        } else {
            console.log("ema: ✍ Patching DOM");
            setHtml(document.documentElement, evt.data);
        }
    };
    window.onbeforeunload = _evt => { ws.close(); };
    window.onpagehide = _evt => { ws.close(); };

  // API
  window.organon = {
    xdgOpen: (fp) => ws.send("organon/open/" + fp)
  };
}
window.onpageshow = _evt => { initShim(false); };
