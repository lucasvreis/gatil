window.addEventListener("DOMContentLoaded", () => {
  const elemToKey = (el) => "fold-data::" + location.pathname + "#" + el.parentElement.id

  const sectionClickListener = (e) => {
    const el = e.currentTarget.parentElement;
    const stKey = elemToKey(el);
    if (!el.hasAttribute("open")) {
      localStorage.setItem(stKey, "true");
    } else {
      localStorage.setItem(stKey, "false");
    }
  }

  const doFold = (el) => {
    const stKey = elemToKey(el)
    switch (localStorage.getItem(stKey)) {
      case "true":
        el.setAttribute("open", "")
        break
      case "false":
        el.removeAttribute("open")
        break
    }
  }

  const doAllToggles = () => {
    document.querySelectorAll("section > details").forEach(el => {
      doFold(el)
      el.firstElementChild.removeEventListener("click", sectionClickListener)
      el.firstElementChild.addEventListener("click", sectionClickListener)
    })
  }

  doAllToggles()

  window.addEventListener("EMAHotReload", doAllToggles)
});
